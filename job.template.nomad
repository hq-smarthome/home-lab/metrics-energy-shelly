job "metrics-energy-shelly" {
  type = "service"
  region = "[[ .defaultRegion ]]"
  datacenters = ["[[ .defaultDatacenter ]]"]
  priority = "[[ .defaultPriority ]]"

  group "plug-s" {
    count = 1

    network {
      mode = "bridge"
    }

    service {
      connect {
        sidecar_service {
          proxy {
            upstreams {
              destination_name = "influxdb"
              local_bind_port = 8086
            }
          }
        }
      }
    }

    task "office-1-air-conditioner" {
      driver = "docker"

      constraint {
        attribute = "${attr.cpu.arch}"
        value = "amd64"
      }

      config {
        image = "[[ .monitorImage ]]"
      }

      resources {
        cpu = 20
        memory = 50
      }

      vault {
        policies = ["energy-monitor"]
      }

      template {
        data = <<EOH
          TZ='[[ .defaultTimezone ]]'
          PUID=[[ .defaultUserId ]]
          PGID=[[ .defaultGroupId ]]

          INFLUXDB_HOST=http://{{ env "NOMAD_UPSTREAM_ADDR_influxdb" }}
          INFLUXDB_BUCKET=Energy

          {{ with secret "energy-monitor/office-1/air-conditioner" }}
          INFLUXDB_ORG={{ index .Data.data "INFLUXDB_ORG" }}
          INFLUXDB_TOKEN={{ index .Data.data "INFLUXDB_TOKEN" }}
          DEVICE_IP={{ index .Data.data "DEVICE_IP" }}
          DEVICE_USERNAME={{ index .Data.data "DEVICE_USERNAME" }}
          DEVICE_PASSWORD={{ index .Data.data "DEVICE_PASSWORD" }}
          DEVICE_ALIAS=Office 1 Air Conditioner
          {{ end }}
        EOH

        destination = "secrets/monitor.env"
        env = true
      }
    }

    task "living-room-1-tv" {
      driver = "docker"

      constraint {
        attribute = "${attr.cpu.arch}"
        value = "amd64"
      }

      config {
        image = "[[ .monitorImage ]]"
      }

      resources {
        cpu = 20
        memory = 50
      }

      vault {
        policies = ["energy-monitor"]
      }

      template {
        data = <<EOH
          TZ='[[ .defaultTimezone ]]'
          PUID=[[ .defaultUserId ]]
          PGID=[[ .defaultGroupId ]]

          INFLUXDB_HOST=http://{{ env "NOMAD_UPSTREAM_ADDR_influxdb" }}
          INFLUXDB_BUCKET=Energy

          {{ with secret "energy-monitor/living-room-1/tv" }}
          INFLUXDB_ORG={{ index .Data.data "INFLUXDB_ORG" }}
          INFLUXDB_TOKEN={{ index .Data.data "INFLUXDB_TOKEN" }}
          DEVICE_IP={{ index .Data.data "DEVICE_IP" }}
          DEVICE_USERNAME={{ index .Data.data "DEVICE_USERNAME" }}
          DEVICE_PASSWORD={{ index .Data.data "DEVICE_PASSWORD" }}
          DEVICE_ALIAS=Living Room 1 TV
          {{ end }}
        EOH

        destination = "secrets/monitor.env"
        env = true
      }
    }

    task "kitchen-1-dishwasher" {
      driver = "docker"

      constraint {
        attribute = "${attr.cpu.arch}"
        value = "amd64"
      }

      config {
        image = "[[ .monitorImage ]]"
      }

      resources {
        cpu = 20
        memory = 50
      }

      vault {
        policies = ["energy-monitor"]
      }

      template {
        data = <<EOH
          TZ='[[ .defaultTimezone ]]'
          PUID=[[ .defaultUserId ]]
          PGID=[[ .defaultGroupId ]]

          INFLUXDB_HOST=http://{{ env "NOMAD_UPSTREAM_ADDR_influxdb" }}
          INFLUXDB_BUCKET=Energy

          {{ with secret "energy-monitor/kitchen-1/dishwasher" }}
          INFLUXDB_ORG={{ index .Data.data "INFLUXDB_ORG" }}
          INFLUXDB_TOKEN={{ index .Data.data "INFLUXDB_TOKEN" }}
          DEVICE_IP={{ index .Data.data "DEVICE_IP" }}
          DEVICE_USERNAME={{ index .Data.data "DEVICE_USERNAME" }}
          DEVICE_PASSWORD={{ index .Data.data "DEVICE_PASSWORD" }}
          DEVICE_ALIAS=Kitchen 1 Dishwasher
          {{ end }}
        EOH

        destination = "secrets/monitor.env"
        env = true
      }
    }
  }

  reschedule {
    delay = "10s"
    delay_function = "exponential"
    max_delay = "10m"
    unlimited = true
  }

  update {
    health_check = "checks"
    min_healthy_time = "10s"
    healthy_deadline = "10m"
    progress_deadline = "15m"
    auto_revert = true
  }

  meta {
    gitSha = "[[ .gitSha ]]"
    gitBranch = "[[ .gitBranch ]]"
    pipelineId = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId = "[[ .projectId ]]"
    projectUrl = "[[ .projectUrl ]]"
  }
}
