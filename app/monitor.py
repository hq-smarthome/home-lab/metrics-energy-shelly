import os
import time
import requests
from influxdb_client import InfluxDBClient, Point
from influxdb_client.client.write_api import SYNCHRONOUS

for var in ["DEVICE_ALIAS", "DEVICE_IP", "DEVICE_USERNAME", "DEVICE_PASSWORD", "INFLUXDB_HOST", "INFLUXDB_ORG", "INFLUXDB_BUCKET", "INFLUXDB_TOKEN"]:
  if var not in os.environ:
    raise EnvironmentError("Failed because {} is not set.".format(var))

influxClient = InfluxDBClient(url=os.environ['INFLUXDB_HOST'], token=os.environ['INFLUXDB_TOKEN'], org=os.environ['INFLUXDB_ORG'])
influxWriteApi = influxClient.write_api(write_options=SYNCHRONOUS)

def getPlugSMeasurements():
  request = requests.get('http://' + os.environ['DEVICE_IP'] + '/status', auth=(os.environ['DEVICE_USERNAME'], os.environ['DEVICE_PASSWORD']))
  timestamp = int(round(time.time() * 1000000000))

  if request.status_code == 200:
    response = request.json()

    mac = response["mac"]
    rssi = response["wifi_sta"]["rssi"]
    power = int(round(response["meters"][0]["power"] * 1000)) # mW

    print('Power: ' + str(power) + 'mW')

    return Point("smart_plug") \
      .tag("model", 'Shelly PlugS') \
      .tag("mac", mac) \
      .tag("rssi", rssi) \
      .tag("alias", os.environ['DEVICE_ALIAS']) \
      .tag("deviceId", mac) \
      .tag("source", "Shelly Plug S") \
      .tag("country", "SE") \
      .field("power", power) \
      .time(timestamp)

  else:
    print('failed to get status')

def measureEnergy():
  point = getPlugSMeasurements()
  influxWriteApi.write(os.environ['INFLUXDB_BUCKET'], os.environ['INFLUXDB_ORG'], point)

# Simple loop for now
while True:
  measureEnergy()
  time.sleep(15)
